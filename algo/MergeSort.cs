﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class MergeSort<T>
    {
        static private List<List<T>> MergeSortParallelism(List<T> ToSort)
        {
            List<List<T>> result = new List<List<T>>();
            var cores = Environment.ProcessorCount;
            var NumberSubList = ToSort.Count() / cores;
            int i = 0;
            for (i = 0; i < ToSort.Count; ++i)
            {
                var index = Math.Min(i / NumberSubList, cores - 1);
                if (index >= result.Count())
                    result.Add(new List<T>());
                result[Math.Min(i / NumberSubList, cores - 1)].Add(ToSort[i]);
            }
            return result;
        }

        static public List<T> ParallelMergeSort(List<T> ToSort)
        {
            var ToSortLists = MergeSortParallelism(ToSort);
            List<T> result = new List<T>();
            Task[] tArray = new Task[ToSortLists.Count];
            for (int i = 0; i < ToSortLists.Count(); ++i)
            {
                var tmp = i;
                tArray[tmp] = new TaskFactory().StartNew(() =>
                {
                    ToSortLists[tmp] = NormalMergeSort(ToSortLists[tmp]);
                });
            }
            Task.WaitAll(tArray);
            foreach (var l in ToSortLists)
            {
                result = Merge(result, l);
            }
            return result;
        }

        //modify List creation into index to work with only one List
        static public List<T> NormalMergeSort(List<T> ToSort)
        {
            if (ToSort.Count <= 1)
                return ToSort;
            else
            {
                List<T> left = new List<T>();
                List<T> right = new List<T>();
                int mid = ToSort.Count / 2;
                int i = 0;
                for (i = 0; i < mid; i++)
                    left.Add(ToSort[i]);
                for (; i < ToSort.Count; i++)
                {
                    right.Add(ToSort[i]);
                }
                return Merge(NormalMergeSort(left), NormalMergeSort(right));
            }
        }

        static private List<T> Merge(List<T> first, List<T> second)
        {
            List<T> result = new List<T>();

            while (first.Count > 0 || second.Count > 0)
            {
                if (first.Count > 0 && second.Count > 0)
                {
                    if (Comparer<T>.Default.Compare(first[0], second[0]) < 0) // first[0] is the smallest
                    {
                        result.Add(first.First());
                        first.Remove(first.First());
                    }
                    else // second is the smallest
                    {
                        result.Add(second.First());
                        second.Remove(second.First());
                    }
                }
                else if (first.Count > 0)
                {
                    result.Add(first.First());
                    first.Remove(first.First());
                }
                else if (second.Count > 0)
                {
                    result.Add(second.First());
                    second.Remove(second.First());
                }
            }
            return result;
        }
    }
}
