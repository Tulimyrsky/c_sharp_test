﻿using Microsoft.VisualBasic.CompilerServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.ComponentModel;

namespace test
{
    class VariousTest
    {
        static public bool StringComparer(string s1, string s2)
        {
            HashSet<char> first = new HashSet<char>(s1);
            HashSet<char> second = new HashSet<char>(s2);

            return first.SetEquals(second);
        }

        public static int[] MergeArrays(int[] x, int[] y)
        {
            int[] result = new int[x.Length + y.Length];
            int xIndex = x.Length - 1;
            int yIndex = y.Length - 1;
            int mergeIndex = result.Length - 1;
            while (yIndex >= 0 && xIndex >= 0)
            {
                if (y[yIndex] > x[xIndex])
                {
                    result[mergeIndex] = y[yIndex];
                    yIndex--;
                }
                else if (y[yIndex] < x[xIndex])
                {
                    result[mergeIndex] = x[xIndex];
                    xIndex--;
                }
                mergeIndex--;
            }
            if (xIndex == 0)
                result[mergeIndex] = x[xIndex];
            else
                result[mergeIndex] = y[yIndex];
            return result;
        }

        //IEnumerable GetComputation(int maxIndex) { for (int i = 0; i < maxIndex; i++) { yield return Computation(i); } }
        //IEnumerable GetComputation1(int maxIndex)
        //{
        //    var result = new int[maxIndex]; 
        //    for (int i = 0; i < maxIndex; i++) 
        //    { 
        //        result[i] = Computation(i); 
        //    }
        //    foreach (var value in result) 
        //    { 
        //        yield return value; 
        //    }
        //}

    }
}
