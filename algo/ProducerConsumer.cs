﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class ProducerConsumer
    {

        static int commandNb = 0;
        private ConcurrentQueue<Food> food = new ConcurrentQueue<Food>();

        class Food
        {
            bool cooked = true;
            int command = commandNb++;

            public int CommandNb()
            {
                return command;
            }
        }
        async void Cooking()
        {
            Console.WriteLine("We prepare your meal");
            await Task.Delay(5000);
            Console.WriteLine("Food is cooked");
            food.Enqueue(new Food());
            Console.WriteLine($"Food is served");
        }
        public void Produce()
        {
            new TaskFactory().StartNew(Cooking);
        }

        public int Consume()
        {
            Food current;
            while(!food.TryDequeue(out current))
            {

            }
            Console.WriteLine($"Meal number {current.CommandNb()} is on the table");
            return current.CommandNb();
        }
    }
}
