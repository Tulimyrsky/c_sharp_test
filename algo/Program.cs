﻿using System;

using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Threading;
using System.ComponentModel;

namespace test
{
    class algo
    {
        static int _thread_nb = 0;
        public static List<int> allmax = new List<int>();
        public static int cores = Environment.ProcessorCount;
        public static int Max(List<int> numbers)
        {
            int result = 0;
            foreach (int i in numbers)
            {
                if (i > result)
                    result = i;
            }
            return result;
        }
        public static void ThreadMax(List<int> numbers)
        {
            int result = 0;
            int th_nr = _thread_nb++;
            for (int i = (numbers.Count / cores) * th_nr; i < (th_nr + 1) * (numbers.Count / cores); i++)
            {
                if (result < numbers[i])
                    result = numbers[i];
            }
            allmax.Add(result);
        }

        public static int ParallelMax(List<int> numbers)
        {
            int result = 0;
            Parallel.ForEach(numbers, new ParallelOptions { MaxDegreeOfParallelism = Environment.ProcessorCount }, i =>
            {
                if (i > result)
                    result = i;
            });
            return result;
        }

        public static int TaskMax(List<int> numbers)
        {
            int result = 0;
            List<int> localmax = new List<int>();
            Console.WriteLine($"Numbers of logical cores : {cores}");
            Task<int>[] tArray = new Task<int>[cores];
            for (int i = 0; i < tArray.Length; i++)
            {
                var ii = i;
                tArray[ii] = new TaskFactory().StartNew(() =>
                {
                    int local_i = ii;
                    int local_result = 0;
                    for (int j = (numbers.Count / cores) * local_i; j < ((local_i + 1) * (numbers.Count / cores)); j++)
                    {
                        if (local_result < numbers[j])
                            local_result = numbers[j];
                    }
                    // localmax.Add(local_result); // not threadsafe
                    return local_result;
                });
            }
            Task.WaitAll(tArray);
            foreach (var t in tArray)
            {
                localmax.Add(t.Result);
            }
            return Max(localmax);
        }
    }

    class Program
    {
        static void TestMax(string[] args)
        {
            Console.WriteLine("Début du programme");
            List<int> numbers = new List<int>();
            Random rnd = new Random();
            for (int i = 0; i < 50000000; ++i)
                numbers.Add(rnd.Next(1, 50000000));
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var res = algo.Max(numbers);
            stopWatch.Stop();
            Console.WriteLine($"Max : le nombre max est {res} et a été trouvé en {stopWatch.Elapsed}.");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            Thread[] t = new Thread[algo.cores];
            for (int i = 0; i < algo.cores; i++)
            {
                t[i] = new Thread(new ThreadStart(() => algo.ThreadMax(numbers)));
                t[i].Start();
            }
            foreach (Thread current in t)
                current.Join();
            res = algo.Max(algo.allmax);
            stopWatch.Stop();
            Console.WriteLine($"ThreadMax : le nombre max est {res} et a été trouvé en {stopWatch.Elapsed}.");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            res = algo.ParallelMax(numbers);
            stopWatch.Stop();
            Console.WriteLine($"ParallelMax : le nombre max est {res} et a été trouvé en {stopWatch.Elapsed}.");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            res = algo.TaskMax(numbers);
            stopWatch.Stop();
            Console.WriteLine($"TaskMax : le nombre max est {res} et a été trouvé en {stopWatch.Elapsed}.");

        }

        static List<int> TestMerge(List<int> numbers)
        {
            Console.WriteLine("Début du MergeSort");
            return MergeSort<int>.NormalMergeSort(numbers);
        }

        static void Main(string[] args)
        {
            TestMax(args);
            List<int> numbers = new List<int>();
            Random rnd = new Random();
            for (int i = 0; i < 50000; ++i)
                numbers.Add(rnd.Next(1, 500));


            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            var r = MergeSort<int>.ParallelMergeSort(numbers);
            stopWatch.Stop();
            Console.WriteLine($"ParallelMergeSort : Algo terminé en {stopWatch.Elapsed}.");

            stopWatch.Start();
            r = MergeSort<int>.NormalMergeSort(numbers);
            stopWatch.Stop();
            Console.WriteLine($"NormalMergeSort : Algo terminé en {stopWatch.Elapsed}.");
        }
    }
}
